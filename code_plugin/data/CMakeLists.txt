### CMakeLists automatically created with AutoVala
### Do not edit

IF(IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/autovalacode.plugin)
	install(DIRECTORY
		${CMAKE_CURRENT_SOURCE_DIR}/autovalacode.plugin
	DESTINATION
		lib/x86_64-linux-gnu/io.elementary.code/plugins/autovala
	)
ELSE()
	install(FILES
		${CMAKE_CURRENT_SOURCE_DIR}/autovalacode.plugin
	DESTINATION
		lib/x86_64-linux-gnu/io.elementary.code/plugins/autovala
	)
ENDIF()

